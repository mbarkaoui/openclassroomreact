import React from 'react'
import {StyleSheet,View, Button, TextInput, FlatList, Text, ActivityIndicator } from 'react-native'
import listfilms from '../Helpers/filmsData.js'
import FilmItem from '../Components/FilmItem.js'
import {getFilmsFromApiWithSearchedText} from '../API/TMDBApi'


class Search extends React.Component {

    constructor(props){
        super(props)
        this.page = 0
        this.totalPages = 0
        this.searchedText =  ""
        this.state = { 
            films: [],
            isLoading: false
         }


    }

    _displayDetailForFilm = (idfilm) => {
        console.log("Display film with id" + idfilm);
                this.props.navigation.navigate("FilmDetail")

    }

    _searchFilms() {
        this.page = 0
        this.totalPages = 0
        this.setState({
            films:[]
        }, () => {
            this._loadFilms() 
        })
    }
    _loadFilms() {
        this.setState({ isLoading: true})
        if (this.searchedText.length > 0){
            getFilmsFromApiWithSearchedText(this.searchedText, this.page+1).then(data => {
              
                this.page = data.page
                this.totalPages = data.total_pages
                this.setState({
                    films: [...this.state.films,...data.results],
                    isLoading: false})
            })
        }  
    }

    _searchTextInputChanged(text) {
        this.searchedText = text
    }

    _displayingLoading() {
        if (this.state.isLoading) {
            return (
                <View style={style.loading_container}>
                    <ActivityIndicator size='large'/>
                </View>
            )
        }
    }


    render() {
        console.log(this.state.isLoading);
        console.log("render");
        return (

            <View style={style.main_container}>
                <TextInput onSubmitEditing = {() => this._searchFilms()} onChangeText = {(text) => this._searchTextInputChanged(text)}  style={style.textInput} placeholder= "Titre du film" />
                <Button color="green" title='Rechercher' onPress = {() => this._searchFilms() }/>
                <FlatList
                  data={this.state.films}
                  keyExtractor={(item) => item.id.toString()}
                  onEndReachedThreshold={0.5}
                  onEndReached={() => {
                      if (this.page < this.totalPages) {
                          this._loadFilms()
                      }
                      console.log("onEndReached")
                  }}
                  renderItem={({item}) => <FilmItem film={item}  displayDetailForFilm={this._displayDetailForFilm} />}
                />
                {this._displayingLoading()}
            </View>
        )
    }
}

const style = StyleSheet.create({
    main_container: {
        flex : 1,
        marginTop : 40
    },
    textInput: {
         marginLeft: 5,
         marginRight: 5, 
         height: 50,
         borderColor: '#000000', 
         borderWidth: 1,
         paddingLeft: 5
    },
    loading_container: {
        position: 'absolute',
        left: 0,
        right: 0,
        top: 100,
        bottom: 0,
        alignItems: 'center',
        justifyContent: 'center'
    }
})

export default Search