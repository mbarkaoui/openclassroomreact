const API_TOKEN = "b785d18dd4c29ff034a3709b6893efc5"

export function getFilmsFromApiWithSearchedText(text,page) {
    const url = 'https://api.themoviedb.org/3/search/movie?api_key=' + API_TOKEN + '&languagefr&query=' + text + '&page=' + page
    return fetch(url)
    .then((response) => response.json())
    .catch((error) => console.log(error)) 

}

export function getImageFromApi(name) {
    return 'https://image.tmdb.org/t/p/w300' + name
}