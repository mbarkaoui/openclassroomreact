import React from 'react';
import Navigation from './Navigation/Navigation'

export default class App extends React.Component {
 render() {
  return (
    <Navigation
    initialRoute={{ name: 'Search' }}
    configureScene={this.configureScene}
/>
  );
  }
}

